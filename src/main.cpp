#include <iostream>
#include <stdio.h>
#include "git2.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <direct.h>
#define GET_CURRENT_DIRECTORY _getcwd
#else
#include <unistd.h>
#define GET_CURRENT_DIRECTORY getcwd
#endif

using namespace std;

int main(int argc, char* argv[]) {
	char working_directory[FILENAME_MAX];
	GET_CURRENT_DIRECTORY(working_directory, FILENAME_MAX);
	cout << working_directory << endl;

	git_libgit2_init();

	git_repository *repo = NULL;

	if(git_repository_init(&repo, working_directory, 0) != 0) {
		cout << "Failed to init repo!" << endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
